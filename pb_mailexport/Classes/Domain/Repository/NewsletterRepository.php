<?php
namespace PlusB\PbMailexport\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Kay Naumann <kay.naumann@plusb.de>, plusB
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for countries
 */
class NewsletterRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

    protected $defaultOrderings = array(
            'tstamp' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
    );


    public function initializeObject(){
        /** @var \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings */
        $querySettings = $this->objectManager->get('\\TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
    }

    public function getNewsletters($newsletterPid) {
        $maxDate = '1460800192';

        $query = $this->createQuery();
        $constraints = $query->logicalAnd(
                            $query->equals('pid', $newsletterPid),
                            $query->greaterThan('tstamp',$maxDate)
                        );

        $query->matching($constraints);
        return $query->execute();
    }

    public function findByNewsletter($newsletterUid){
        $query = $this->createQuery();
        $statement = "SELECT DISTINCT first_name, last_name , title, a.email, a.uid, a.pid, a.hidden FROM sys_dmail_maillog d JOIN tt_address a on d.rid = a.uid WHERE d.html_sent = 3 AND d.rid LIKE a.uid AND d.mid LIKE ".$newsletterUid;
        $query = $query->statement($statement);
        $query->getQuerySettings()->setReturnRawQueryResult(TRUE);

        $mailsSent = $query->execute();
        $returnMails = $this->findReturnedMails($newsletterUid);
        $openedMails = $this->findOpenedMails($newsletterUid);

        $dictionaryOpenedMails = array();
        foreach ($openedMails as $mail) {
            $dictionaryOpenedMails[$mail['uid']] = $mail['uid'];
        }

        $dictionaryReturnedMails = array();
        foreach($returnMails as $mail) {
            $dictionaryReturnedMails[$mail['uid']] = $mail['uid'];
        }

        $maxReturnedMails = count($returnMails);
        $maxMailsSent = count($mailsSent);

        for($m = 0; $m < $maxMailsSent; $m++) {
            if (array_key_exists($mailsSent[$m]['uid'], $dictionaryOpenedMails)) {
                $mailsSent[$m]['ge�ffnet'] = 'Ja';
            }
            else {
                $mailsSent[$m]['ge�ffnet'] = 'Nein';
            }
        }

        for($m = 0; $m < $maxMailsSent; $m++) {
            if(array_key_exists($mailsSent[$m]['uid'], $dictionaryReturnedMails)){
                unset($mailsSent[$m]);
                $maxMailsSent--;
            }
        }

        $maxOpenedMails = count($openedMails);
        return $mailsSent;
    }


    public function findReturnedMails($newsletterUid){
        $query = $this->createQuery();
        $statement = "SELECT DISTINCT first_name, last_name , title, a.email, a.uid, a.pid , a.hidden FROM sys_dmail_maillog d JOIN tt_address a on d.rid = a.uid WHERE d.response_type = -127 AND d.mid = ".$newsletterUid;
        $query = $query->statement($statement);
        $query->getQuerySettings()->setReturnRawQueryResult(TRUE);
        return $query->execute();
    }

    public function findOpenedMails($newsletterUid) {
        $query = $this->createQuery();
        $statement = "SELECT DISTINCT first_name, last_name , title, a.email, a.uid, a.pid , a.hidden FROM sys_dmail_maillog d JOIN tt_address a on d.rid = a.uid WHERE d.response_type = -1 AND d.mid = ".$newsletterUid;
        $query = $query->statement($statement);
        $query->getQuerySettings()->setReturnRawQueryResult(TRUE);
        return $query->execute();
    }
}


