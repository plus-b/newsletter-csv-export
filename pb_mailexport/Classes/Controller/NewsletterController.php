<?php
namespace PlusB\PbMailexport\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Kay Naumann <kay.naumann@plusb.de>, PlusB
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * NewsletterController
 */
class NewsletterController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {


	/**
	 * newsletterRepository
	 *
	 * @var \PlusB\PbMailexport\Domain\Repository\NewsletterRepository
	 * @inject
	 */
	protected $newsletterRepository;

	/**
	 * RecipientRepository
	 *
	 * @var \PlusB\PbMailexport\Domain\Repository\RecipientRepository
	 * @inject
	 */
	protected $recipientRepository;


	/**
	 * action list
	 * 
	 * @return void
	 */
	public function listAction() {
        $user= $GLOBALS["BE_USER"]->user["uid"];

        //DebuggerUtility::var_dump($user);
        switch($user) {
            case 8:
                $newsletter = $this->newsletterRepository->getNewsletters('37');
                break;
            case 7:
                $newsletter = $this->newsletterRepository->getNewsletters('24');
                break;
            case 4:
                $newsletter = $this->newsletterRepository->getNewsletters('2');
                break;
            default:
                $newsletter = $this->newsletterRepository->findAll();
        }

		$this->view->assign('newsletters', $newsletter);
	}

	/**
	 * action show
	 * @param \PlusB\PbMailexport\Domain\Model\Newsletter $newsletter
     * @return void
	 */
	public function showAction(\PlusB\PbMailexport\Domain\Model\Newsletter $newsletter) {
        $this->view->assign('newsletter', $newsletter);
    }

	/**
	 * action csvExport
	 *
     * @param \PlusB\PbMailexport\Domain\Model\Newsletter $newsletter
     *
     * @return void
	 */
	public function csvExportAction(\PlusB\PbMailexport\Domain\Model\Newsletter $newsletter) {

        $recipients = $this->newsletterRepository->findByNewsletter($newsletter->getUid());


        $now = time();
        $downloadDate = strftime("%Y%m%d", $now);

        $colums = array( 0 => array(
                        0 => "first_name",
                        1 => "last_name",
                        2 => "title",
                        3 => "email",
                        4 => "uid",
                        5 => "pid",
                        6 => "hidden",
                        7 => "opened"
                    )
                );

        $mergedRecipients = array_merge($colums, $recipients);

	    $downloadfile = PATH_site . "typo3temp/mailexport.csv";

        $csvFile = fopen($downloadfile, "w");
        foreach ($mergedRecipients as $fields) {
            fputcsv($csvFile, $fields,';','"');
        }

        fclose($csvFile);

        // do all the export stuff here...

        // ----------------------------------------
        // download csv file
        // ----------------------------------------
        // prepare infos
        $cType = 'application/csv';
        //$fileLen = filesize($exportFileName);
        $headers = array(
                'Pragma' => 'public',
                'Expires' => 0,
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Cache-Control' => 'public',
                'Content-Description' => 'File Transfer',
                'Content-Type' => $cType,
                'Content-Disposition' => 'attachment; filename = "'.$downloadDate.'_mailexport.csv"',
                'Content-Transfer-Encoding' => 'binary'

        );

        // send headers
        foreach ($headers as $header => $data)
            $this->response->setHeader($header, $data);

        $this->response->sendHeaders();
        // send file
        @readfile($downloadfile);

        unlink($downloadfile);

        exit;
    }


	///**
	// * Lifecycle-Event
	// * wird nach der Initialisierung des Objekts und nach dem Aufl�sen der Dependencies aufgerufen.
	// *
	// */
	//public function initializeObject() {
	//	$this->databaseHandle = $GLOBALS['TYPO3_DB'];
	//	$this->databaseHandle->explainOutput = 2;
	//	$this->databaseHandle->store_lastBuiltQuery = TRUE;
	//	$this->databaseHandle->debugOutput = 2;
	//}
    //
	///**
	// * The TYPO3 database object
	// *
	// * @var \TYPO3\CMS\Core\Database\DatabaseConnection
	// *
	// */
	//protected $databaseHandle;

}