<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:pb_mailexport/Resources/Private/Language/locallang_db.xlf:tx_pbmailexport_domain_model_person',
		'label' => 'uid',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',

		'enablecolumns' => array(

		),
		'searchFields' => 'name',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('pb_mailexport') . 'Resources/Public/Icons/tx_pbmailexport_domain_model_person.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, firstname, lastname, email, newsletteropened',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, firstname, lastname, email, newsletteropened'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

			'firstname' => array(
					'exclude' => 0,
					'label' => 'LLL:EXT:pb_mailexport/Resources/Private/Language/locallang_db.xlf:tx_pbmailexport_domain_model_person.firstname',
					'config' => array(
							'type' => 'input',
							'size' => 100,
							'eval' => 'null',
							'autocomplete' => false,

					)
			),

			'lastname' => array(
					'exclude' => 0,
					'label' => 'LLL:EXT:pb_mailexport/Resources/Private/Language/locallang_db.xlf:tx_pbmailexport_domain_model_newsletter.lastname',
					'config' => array(
							'type' => 'input',
							'size' => 100,
							'eval' => 'null',
							'autocomplete' => false,
					)
			),
			'email' => array(
					'exclude' => 0,
					'label' => 'LLL:EXT:pb_mailexport/Resources/Private/Language/locallang_db.xlf:tx_pbmailexport_domain_model_newsletter.email',
					'config' => array(
							'type' => 'input',
							'size' => 100,
							'eval' => 'null',
							'autocomplete' => false,
					)
			),

			'newsletteropened' => array(
					'exclude' => 0,
					'label' => 'LLL:EXT:pb_mailexport/Resources/Private/Language/locallang_db.xlf:tx_pbmailexport_domain_model_newsletter.newsletteropened',
					'config' => array(
							'type' => 'input',
							'size' => 100,
							'eval' => 'null',
							'autocomplete' => false,
					)
			),




		//	'sys_language_uid' => array(
		//	'exclude' => 1,
		//	'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
		//	'config' => array(
		//		'type' => 'select',
		//		'foreign_table' => 'sys_language',
		//		'foreign_table_where' => 'ORDER BY sys_language.title',
		//		'items' => array(
		//			array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
		//			array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
		//		),
		//	),
		//),
		//'l10n_parent' => array(
		//	'displayCond' => 'FIELD:sys_language_uid:>:0',
		//	'exclude' => 1,
		//	'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
		//	'config' => array(
		//		'type' => 'select',
		//		'items' => array(
		//			array('', 0),
		//		),
		//		'foreign_table' => 'tx_pbmailexport_domain_model_person',
		//		'foreign_table_where' => 'AND tx_pbmailexport_domain_model_person.pid=###CURRENT_PID### AND tx_pbmailexport_domain_model_person.sys_language_uid IN (-1,0)',
		//	),
		//),
		//'l10n_diffsource' => array(
		//	'config' => array(
		//		'type' => 'passthrough',
		//	),
		//),
        //
		//'t3ver_label' => array(
		//	'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
		//	'config' => array(
		//		'type' => 'input',
		//		'size' => 30,
		//		'max' => 255,
		//	)
		//),

	),
);