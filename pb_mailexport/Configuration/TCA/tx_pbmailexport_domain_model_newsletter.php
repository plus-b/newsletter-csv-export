<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:pb_mailexport/Resources/Private/Language/locallang_db.xlf:tx_pbmailexport_domain_model_newsletter',
		'label' => 'uid',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',

		'enablecolumns' => array(

		),
		'searchFields' => 'name',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('pb_mailexport') . 'Resources/Public/Icons/tx_pbmailexport_domain_model_newsletter.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource,title, date, recipient',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource,title, date, recipient'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

			'title' => array(
					'exclude' => 0,
					'label' => 'LLL:EXT:pb_mailexport/Resources/Private/Language/locallang_db.xlf:tx_pbmailexport_domain_model_newsletter.title',
					'config' => array(
							'type' => 'input',
							'size' => 100,
							'eval' => 'null',
							'autocomplete' => false,
					)
			),
			'date' => array(
					'exclude' => 0,
					'label' => 'LLL:EXT:pb_mailexport/Resources/Private/Language/locallang_db.xlf:tx_pbmailexport_domain_model_newsletter.date',
					'config' => array(
							'type' => 'input',
							'size' => 200,
							'eval' => 'null',
							'autocomplete' => false,
					)
			),
			'recipient' => array(
					'exclude' => 0,
					'label' => 'LLL:EXT:pb_mailexport/Resources/Private/Language/locallang_db.xlf:tx_pbmailexport_domain_model_newsletter.recipient',
					'config' => array(
							'type' => 'group',
							'internal_type' => 'db',
							'allowed' => 'tt_address',
							'size' => '5',
							'minitems' => '0',
							'show_thumbs' => '1',
					)
			),



		//	'sys_language_uid' => array(
		//	'exclude' => 1,
		//	'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
		//	'config' => array(
		//		'type' => 'select',
		//		'foreign_table' => 'sys_language',
		//		'foreign_table_where' => 'ORDER BY sys_language.title',
		//		'items' => array(
		//			array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
		//			array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
		//		),
		//	),
		//),
		//'l10n_parent' => array(
		//	'displayCond' => 'FIELD:sys_language_uid:>:0',
		//	'exclude' => 1,
		//	'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
		//	'config' => array(
		//		'type' => 'select',
		//		'items' => array(
		//			array('', 0),
		//		),
		//		'foreign_table' => 'tx_pbmailexport_domain_model_newsletter',
		//		'foreign_table_where' => 'AND tx_pbmailexport_domain_model_newsletter.pid=###CURRENT_PID### AND tx_pbmailexport_domain_model_newsletter.sys_language_uid IN (-1,0)',
		//	),
		//),
		//'l10n_diffsource' => array(
		//	'config' => array(
		//		'type' => 'passthrough',
		//	),
		//),
        //
		//'t3ver_label' => array(
		//	'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
		//	'config' => array(
		//		'type' => 'input',
		//		'size' => 30,
		//		'max' => 255,
		//	)
		//),

	),
);